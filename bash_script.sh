# update the repositories 
sudo yum update -y

# install httpd
sudo yum install -y httpd

# start httpd
sudo systemctl start httpd

# Move the file into the correct location
sudo mv ~/kofi_website/kofi_index.html /var/www/html/index.html

# restart httpd
sudo systemctl restart httpd