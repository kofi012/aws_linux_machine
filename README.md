## Commands for running bash script remotely on AWS machine

```bash
# command to send script file

 scp -i <secure_key> <file> <user>@<IP_address>:<path_directory>

 scp -i ~/.ssh/ch9_shared.pem bash_script.sh ec2-user@:/home/ec2-user/kofi.sh

# command to send html for website

 scp -i <secure_key> -r <folder> <user>@<IP_address>:<file_directory>

scp -i ~/.ssh/ch9_shared.pem -r kofi1_website ec2-user@54.228.125.171:/home/ec2-user/

 # command to run bash script

 ssh -i <secure_key> <user>@<IP_address>: bash <bash_file_path>

 ssh -i ~/.ssh/ch9_shared.pem ec2-user@54.228.125.171 bash /home/ec2-user/kofi.sh

 # moving files on httpd

 sudo mv ~/kofi1_website/kofi_index.html /var/www/html/
 ```